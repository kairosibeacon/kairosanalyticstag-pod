//
//  Copyright Â© 2016 Kairos. All rights reserved.
//

// Interface v1.4
/////////////////////

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define kAllowKTracking @"com.k.allowTracking"

typedef void (^completion_block_t)(id __nullable error, id __nullable result);

@protocol KairosAnalyticsTagDelegate <NSObject>

-(void) onError:(NSString * __nullable) message;

@end

@interface KairosAnalyticsTag : NSObject<CLLocationManagerDelegate>


/**
 *  When the beacon manager enters a region,
 * it actively scans for beacons during the scanDelay.
 * The default scan delay is 8 seconds and can be tweaked by setting directly this value.
 */
@property (assign, nonatomic) NSInteger scanDelay;

/**
 *  Sets the beacon manager delegate.
 */
@property (strong, nonatomic, nullable) id<KairosAnalyticsTagDelegate> delegate;

/**
 * Get KairosBeaconManager singleton
 *
 * @return KairosbeaconManager singleton
 */
+ (KairosAnalyticsTag * __nullable)sharedInstance:(NSString * __nonnull) secret
                                         appToken:(NSString * __nonnull) appToken
                                         delegate:(id<KairosAnalyticsTagDelegate> __nonnull) delegate;


/**
 * When set to YES, shows KairosAnalyticsTag logs
 *
 * @param debug <#debug description#>
 */
-(void) setDebug:(BOOL) debug;

/**
 * Start beacon region monitoring
 */
-(void) startBeaconMonitoring;

/**
 * Force beacon ranging.
 * This can be use to detect more actively beacons but is a little bit more energy eating.
 * Typical use case : you can force ranging when app triggers -(void)applicationDiDBecomeActive
 */
-(void) startRanging;

/**
 * When set to NO, tracking is disabled, beacon monitoring is disabled.
 * There won't be any tracking data sent htrough network nor beacon monitoring from kairos.
 * Nota bene: you can directly disable the sdk through NSUserDefault when setting boolean "kAllowKTracking" to NO.
 *
 * @param enable YES / NO
 */
-(void) setTrackingEnabled:(BOOL) value;

/**
 * Check wether tracking is enabled or not
 *
 * return BOOL
 */
-(BOOL) hasTrackingEnabled;

/**
 * This method is aimed at helping app editors and developpers
 * check wether the app should ask again for user tracking authorization.
 * If the user accept tracking again, refreshOptinTrackingDate must be called.
 *
 * return BOOL
 */
-(BOOL) shouldAskTracking;

/**
 * Refresh next authorization date for user tracking
 */
-(void) refreshOptinTrackingDate;

/**
 * Set max tracking interval time to match app country regulation.
 * Default is 13 months (french regulation).
 * When set to 0, tracking interval is considered infinity.
 */
-(void) setMaxTrackingInterval:(NSTimeInterval) interval;

@end
