# Documentation technique KairosFire Analytics Tag

![Kairosfire](https://bytebucket.org/kairosibeacon/kairosanalyticstag-pod/raw/master/doc/kairosfire-rgb.png)


## Guide d'intégration du TAG sous Xcode / iOS

### Etape 1: Installation du TAG

#### Installation via cocoapods

Nous conseillons fortement une installation via cocoapods, si celui-ci n'est pas déjà installé, lancez cette commande dans le terminal : ```sudo gem install cocoapods```.

Créez ou complétez ensuite le fichier Podfile à la racine de votre projet Xcode :


Pour Cocoapods v1.0 et plus :

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/kairosibeacon/kairosanalyticstag-pod.git'

platform :ios, '7.0'
target your_target 
  pod 'KairosAnalyticsTag', '~> 1.4.0'
```


Pour les versions de Cocoapods < 1.0 :

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/kairosibeacon/kairosanalyticstag-pod.git'

platform :ios, '7.0'
pod 'KairosAnalyticsTag', '~> 1.4.0'
```

Exécutez ensuite la commande ```pod install```.

> **⚠️ Attention :** 
> À partir de cette étape, il faudra dorénavant utiliser le fichier ```.xcworkspace``` pour ouvrir votre projet Xcode à la place de ```.xcproj```

#### Installation manuelle du TAG

##### Intégration de la librairie 

Vous devez copier les fichiers ```libKairosAnalyticsTag.a```, ```KExtrasResources.bundle``` et ```KairosAnalyticsTag.h``` dans le projet courant. Le plus simple est de glisser-déposer les fichiers dans l'interface de Xcode.
Ces fichiers sont téléchargeable à cette [adresse](https://bitbucket.org/kairosibeacon/kairosanalyticstag-pod/downloads).


### Etape 2 : Edition du fichier Plist

L'édition du fichier Plist permet de personnaliser le message de l'écran de demande d'autorisation d'accès aux données de localisation en tâche de fond de l'application.

> ⚠️ Cette modification est **nécessaire** pour le bon fonctionnement du TAG Kairos.

Il est possible de laisser une chaîne de caractères vide, cependant, **nous incitons fortement l'application mobile à utiliser cet espace pour expliquer à l'utilisateur la valeur ajoutée de l'autorisation**.

Dans le fichier PList du projet, rajoutez la clef ```NSLocationAlwaysUsageDescription``` avec en valeur un texte qui invitera l'utilisateur à autoriser l'application à mobile à accéder aux données de localisation en tâche de fond.

Voici un exemple de fenêtre popup qui s'affichera, "Votre Application" sera remplacé par le nom de votre application et le texte intégré dans votre fichier PList sera affiché à la suite de la popup :

![popup](https://bytebucket.org/kairosibeacon/kairosbeaconmanager-pod/raw/9a67103bb755ec3e93cf7ab8e2c6e27d3aa6716b/doc/notification3.png)

### Etape 3 : Modification du fichier AppDelegate.m
Pour initialiser le SDK, il est nécessaire d'ajouter les lignes suivantes dans la méthode  ```didFinishLaunchingWithOptions```:
> **⚠️ Attention :** 
> Ne pas oublier de remplacer ```app_secret``` et ```app_token``` par les valeurs fournies.


```objectivec
#import "AppDelegate.h"
#import "KairosAnalyticsTag.h"

/** ⚠️ Mandatory : set protocol for delegate **/
@interface AppDelegate()<KairosAnalyticsTagDelegate>

@property (strong, nonatomic) KairosAnalyticsTag *manager;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication*)application 
didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
     /** ⚠️ Mandatory : initialize analytics tag **/
    self.manager = [KairosAnalyticsTag sharedInstance:@"app_secret" 
                                         appToken:@"app_token" 
                                         delegate:self];
    
    // optional
    [self.manager setDebug:YES];
    
    // ⚠️ mandatory start monitoring for kairos beacons
    [self.manager startBeaconMonitoring];
    
    return YES;
}
```


### Etape 4 (optionnelle) : Recherche active de balises iBeacon

Par défaut, le TAG écoute de manière passive les iBeacons à proximité (mode monitoring actif tout le temps, y compris lorsque l'application est en background). 
En utilisant la méthode ```startRanging``` on peut demander au TAG d'effectuer une recherche plus aggressive des iBeacons à proximité, cette méthode ne fonctionnera que lorsque l'application est active.  
Pour activer ce mode, on peut ajouter la ligne suivante dans la méthode ```applicationDidBecomeActive``` :

```objectivec
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self.manager startRanging];
}

```

```startRanging``` va activer le ranging de balises iBeacon pendant la durée ```scanDelay``` (voir Annexe > API).

## Changelog 

### Changements (v1.4.8)

* Amélioration de la gestion des paramètres de vie privée

### Changements (v1.4.7)

* Suppression de la dépendance AFNetworking
* Résolution de bugs mineurs

### Changements (v1.4.6)

* Supression de la dépendance sur GoogleAnalytics
* Amélioration du comportement du SDK en environnement multi SDK
* Compilation de la librairie pour ios 7.0 minimum
* Amélioration de la remontée de statistiques
* Résolution de bugs majeurs 

## Annexe

### API :

Les méthodes publiques sont disponible dans l'interface [KairosAnalyticsTag.h](https://bitbucket.org/kairosibeacon/kairosanalyticstag-pod/src/master/src/KairosAnalyticsTag.h?at=master&fileviewer=file-view-default).

## Contacts :

Technique : [Alexandre  Assouad](mailto:assouad@kairosfire.com)

Responsable partenariats : [Pierre-Louis Picot](mailto:picot@kairosfire.com)

